package service

import (
	log "github.com/sirupsen/logrus"

	"gitlab.reutlingen-university.de/frthih23/myaktion-go/src/myaktion/db"
	"gitlab.reutlingen-university.de/frthih23/myaktion-go/src/myaktion/model"
)

func CreateCampaign(campaign *model.Campaign) error {
	result := db.DB.Create(campaign)
	if result.Error != nil {
		return result.Error
	}
	log.Infof("Successfully stored new campaign with ID %v in database.", campaign.ID)
	log.Tracef("Stored: %v", campaign)
	return nil
}

func GetCampaigns() ([]model.Campaign, error) {
	var campaigns []model.Campaign
	result := db.DB.Preload("Donations").Find(&campaigns)
	if result.Error != nil {
		return nil, result.Error
	}
	log.Tracef("Retrieved: %v", campaigns)
	return campaigns, nil
}

func GetCampaign(id uint) (*model.Campaign, error) {
	var campaign model.Campaign
	result := db.DB.Preload("Donations").First(&campaign, id)
	if result.Error != nil {
		return nil, result.Error
	}
	log.Tracef("Retrieved: %v", campaign)
	return &campaign, nil
}

func UpdateCampaign(id uint, updatedCampaign *model.Campaign) error {
	var campaign model.Campaign
	result := db.DB.First(&campaign, id)
	if result.Error != nil {
		return result.Error
	}

	campaign.Name = updatedCampaign.Name
	campaign.OrganizerName = updatedCampaign.OrganizerName
	campaign.TargetAmount = updatedCampaign.TargetAmount
	campaign.DonationMinimum = updatedCampaign.DonationMinimum

	result = db.DB.Save(&campaign)
	if result.Error != nil {
		return result.Error
	}
	log.Infof("Successfully updated new campaign with ID %v in database.", campaign.ID)
	log.Tracef("Updated: %v", campaign)
	return nil
}

func DeleteCampaign(id uint) error {
	result := db.DB.Delete(&model.Campaign{}, id)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func AddDonation(id uint, donation *model.Donation) (*model.Campaign, error) {
	var campaign model.Campaign
	result := db.DB.Preload("Donations").First(&campaign, id)
	if result.Error != nil {
		return nil, result.Error
	}
	campaign.Donations = append(campaign.Donations, *donation)
	result = db.DB.Save(&campaign)
	if result.Error != nil {
		return nil, result.Error
	}
	return &campaign, nil
}
