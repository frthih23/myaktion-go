package handler

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.reutlingen-university.de/frthih23/myaktion-go/src/myaktion/model"
	"gitlab.reutlingen-university.de/frthih23/myaktion-go/src/myaktion/service"
)

func CreateCampaign(w http.ResponseWriter, r *http.Request) {
	var campaign *model.Campaign
	campaign, err := getCampaign(r)
	if err != nil {
		log.Printf("Can't serialize request body to campaign struct: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateCampaign(campaign); err != nil {
		log.Printf("Error calling service CreateCampaign: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, campaign)
}

func GetCampaigns(w http.ResponseWriter, _ *http.Request) {
	campaigns, err := service.GetCampaigns()
	if err != nil {
		log.Printf("Error calling service GetCampaigns: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, campaigns)
}

func GetCampaign(w http.ResponseWriter, r *http.Request) {
	var campaign *model.Campaign
	id, err := getId(r)
	if err != nil {
		log.Printf("Can't get ID from request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	campaign, err = service.GetCampaign(id)
	if err != nil {
		log.Printf("Error calling service GetCampaign: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, campaign)
}

func UpdateCampaign(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		log.Printf("Can't get ID from request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	campaign, err := getCampaign(r)
	if err != nil {
		log.Printf("Can't serialize request body to campaign struct: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.UpdateCampaign(id, campaign); err != nil {
		log.Printf("Error calling service UpdateCampaign: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, campaign)
}

func DeleteCampaign(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		log.Printf("Can't get ID from request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.DeleteCampaign(id); err != nil {
		log.Printf("Error deleting campaign: %v", err)
		http.Error(w, "Failed to delete campaign", http.StatusInternalServerError)
		return
	}
	response := Result{
		Success: "Campaign deleted successfully",
	}
	sendJson(w, response)
}

func AddDonation(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		log.Printf("Can't get ID from request: %v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var donation model.Donation
	if err := json.NewDecoder(r.Body).Decode(&donation); err != nil {
		http.Error(w, "Failed to decode donation data", http.StatusBadRequest)
		return
	}
	campaign, err := service.AddDonation(id, &donation)
	if err != nil {
		log.Printf("Error adding donation: %v", err)
		http.Error(w, "Failed to add donation", http.StatusInternalServerError)
		return
	}
	sendJson(w, campaign)
}

func getCampaign(r *http.Request) (*model.Campaign, error) {
	var campaign model.Campaign
	err := json.NewDecoder(r.Body).Decode(&campaign)
	if err != nil {
		log.Errorf("Can't serialize request body to campaign struct: %v", err)
		return nil, err
	}
	return &campaign, nil
}
